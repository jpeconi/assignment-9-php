<?php
/****************************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-04-17
 * Assignment 9
 * Internet Programming 1
 *
 * This page contains a form which allows the user to search for an employee
 * in the database. The user can search either by first name, last name, or
 * both. This page also contains options on how the user would like the data
 * displayed on the page. It contains options for how to order the results,
 * the limit of results returned, and whether or not its in ascending or
 * descending order. This form submits data to the employee-search-results
 * page.
 *
 *****************************************************************************/

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Search | Assignment 9</title>
    <link href="custom.css" rel="stylesheet">
</head>
    <body>
        <div id="employeeArea">
            <fieldset>
                <legend>Employee Search</legend>
                <form method="get" action="employee-search-results.php">
                    <label for="firstName">First Name</label>
                    <input type="text" id="firstName" name="fName">
                    <br>
                    <br>
                    <label for="lastName">Last Name</label>
                    <input type="text" id="lastName" name="lName">
                    <br>
                    <br>
                    <label for="items">Limit results</label>
                    <select id="items" name="items">
                        <option name="items" value="25">25</option>
                        <option name="items" value="100" selected>100</option>
                        <option name="items" value="250">250</option>
                        <option name="items" value="500">500</option>
                        <option name="items" value="750">750</option>
                    </select>
                    <label for="sort">Sort results by</label>
                    <select id="sort" name="sort">
                        <option name="sort" value="emp_no">Employee Number</option>
                        <option name="sort" value="first_name">First Name</option>
                        <option name="sort" value="last_name" selected>Last Name</option>
                        <option name="sort" value="birth_date">Birth Date</option>
                    </select>
                    <br>
                    <br>
                    <label for="order">Order by</label>
                    <select id="order" name="order">
                        <option name="order" value="ASC" selected>Ascending</option>
                        <option name="order" value="DESC">Descending</option>
                    </select>
                    <br>
                    <br>
                    <input type="submit" value="Submit">
                </form>
            </fieldset>
        </div>
    </body>
</html>
