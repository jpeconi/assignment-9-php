<?php
/******************************************************************
 * Author: Jamison Peconi
 * Date: 2016-04-08
 * Assignment 8 - Employee Database
 * Internet Programming 1
 *
 * This page makes a call to the employees database. This page
 * takes an id passed in from a GET request in the URL. It then
 * runs that id into some queries to extract data from the database
 * and output it to the page. This page also handles errors in the
 * even that no parameter is passed in with the URL.
 *
 ******************************************************************/

    // Declare a boolean value to be set if no ID is set
    $isIdSet = true;
    // Logic to handle if an ID has not been sent in the URL
    if (!isset($_GET['id'])) {
        // Set the variable to false
        $isIdSet = false;
        // Connect to the database
        @ $db = mysqli_connect('localhost', 'root', 'root', 'employees');
        // Get the total number of employee records
        $empTotalQuery = "SELECT * FROM employees";
        // Capture the results from the query
        $totalResults = $db->query($empTotalQuery);
        // Set the variable to the total number of results to be displayed below
        $total = $totalResults->num_rows;
        // Free the results
        $totalResults->free();
        // Close the database connection
        $db->close();
    } else {
        // Set the id to a variable if a GET request is sent
        $id = $_GET['id'];
        // Make conection to the database
        @ $db = new mysqli('localhost', 'root', 'root', 'employees');
        // Gives the user an error if the conection to the database could not be made
        if (mysqli_connect_errno()) {
            echo 'Error: Could not connect to database. Please try again later.';
            echo "</body></html>";
            exit;
        }
        // Build the query to find the employee and store it to a variable
        $empQuery = "SELECT * FROM employees where emp_no = $id";
        // Store the result of the query into a variable
        $employeeResult = $db->query($empQuery);
        // Get the number of rows or results returned from the result object
        $numResults = $employeeResult->num_rows;
        //print_r($numResults);
        $isResults = true;

        // If no results are returned trigger a flag to be used below to output an error message
        if ($numResults < 1) {
            $isResults = false;
        // If there are results returned then continue with the query and processing of the database
        } else {
            // Declare a row which contains the fields for an employee
            $empRow = $employeeResult->fetch_assoc();
            // Declare variables based on the fields returned to empRow
            $firstName = $empRow['first_name'];
            $lastName = $empRow['last_name'];
            $gender = $empRow['gender'];
            $birthDate = $empRow['birth_date'];
            $hireDate = $empRow['hire_date'];
            // Query to find the job titles for the emp id
            $jobTitleQuery = "SELECT title FROM titles WHERE emp_no =$id";
            // Query to find the 4 salaries for the employee with the emp id in the GET request
            $salaryQuery = "SELECT salary,from_date,to_date FROM salaries WHERE emp_no= $id ORDER BY from_date DESC LIMIT 4";
            // Query to find the department name of the employee
            $departmentQuery = "SELECT dept_name \n"
                . "FROM departments d, dept_emp de\n"
                . "WHERE d.dept_no = de.dept_no\n"
                . "AND de.emp_no = 10001";
            // Result of the department query
            $deptResult = $db->query($departmentQuery);
            $deptRow = $deptResult->fetch_assoc();
            $department = $deptRow['dept_name'];
            // Results of the title query
            $titleResult = $db->query($jobTitleQuery);
            // Results of the salary query
            $salaryResult = $db->query($salaryQuery);
            // Grab the title from the title results query
            $titleRow = $titleResult->fetch_assoc();
            $title = $titleRow['title'];
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Information Page</title>
    <link href="custom.css" rel="stylesheet">
</head>
<body>
    <div id="employeeArea">
        <?php
            // If no ID is added to the URL, display an error message! Then kill the script.
            if(!$isIdSet) {
                echo "No ID entered!";
                echo "<br>There are $total employees in the database</div></body></html>";
                die;
            }
            // If no results are returned give the user an error message
            if (!$isResults) {
                echo "Sorry no employees were found!</div></body></html>";
                die;
            } else {
            // If a result is found output the results
        ?>
            <span class="bold"><?php echo $firstName . " " . $lastName . ", " . $title ?></span>
            <p><span class="bold">Hired: </span><?php echo $hireDate ?></p>
            <p><span class="bold">Gender: </span><?php echo $gender ?></p>
            <p><span class="bold">DOB: </span><?php echo $birthDate ?></p>
            <p><span class="bold">Department: </span><?php echo $department ?></p>
            <table id="salaryTable">
                <tr>
                    <th>Salary</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
                <?php
                // Loop through the rows returned from the salary query. Print the results in a table.
                while ($salRow = $salaryResult->fetch_assoc()) {
                    // Statement to change the date to a - if they are still currently employed
                    if (substr($salRow['to_date'], 0, 4) == '9999') {
                        $salRow['to_date'] = "-";
                    }
                    // Create a row and columns for every result
                    echo "<tr>";
                    echo "<td>" . "$" . $salRow['salary'] . "</td>";
                    echo "<td>" . $salRow['from_date'] . "</td>";
                    echo "<td>" . $salRow['to_date'] . "</td>";
                    echo "</tr>";
                }
                ?>
            </table>
                <a href="javascript:history.back()">Go back to results</a>
                <a href="employee-search.php">Return to search page</a>
            <?php
                // Free up all the results
                $employeeResult->free();
                $salaryResult->free();
                $deptResult->free();
                $titleResult->free();
                // Close the connection to the database
                $db->close();
        }
            ?>
    </div>
</body>
</html>
