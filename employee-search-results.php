<?php
/****************************************************************************
 * Created by PhpStorm.
 * User: Jamison Peconi
 * Date: 2016-04-17
 * Assignment 9
 * Internet Programming 1
 *
 * This is the search results page. This page handles all the processing.
 * This page takes the inputs gathered from the first page and uses them
 * to build a query which will be sent to the database to get a result
 * set. This page makes a connection to the employees database at localhost.
 * The data returned is neatly displayed in a table.
 *
 *****************************************************************************/

    // Connect to the database
    @ $db = mysqli_connect('localhost', 'root', 'root', 'employees');
    // Display an error in the event a connection cannot be made
    if (mysqli_connect_errno()) {
        echo "Could not connect to database!";
        die;
    }

    // Declare the variables
    $fName = mysqli_real_escape_string($db,$_GET['fName']);
    $lName = mysqli_real_escape_string($db,$_GET['lName']);
    $numResults = mysqli_real_escape_string($db,$_GET['items']);
    $sortType = mysqli_real_escape_string($db,$_GET['sort']);
    $order = mysqli_real_escape_string($db,strtoupper($_GET['order']));

    //Create the query
    $query = "SELECT * FROM employees WHERE first_name LIKE '" . $fName . "%' AND 
    last_name LIKE '%" . $lName . "%' ORDER BY " . $sortType . " " . $order . " LIMIT " . $numResults;
    // Capture the results of the query into a variable
    $fNameResult = $db->query($query);
    // Get the number of results
    $numResults = $fNameResult->num_rows;
    
    //print_r($query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Search | Assignment 9</title>
    <link href="custom.css" rel="stylesheet">
</head>
    <body>
        <div id="employeeArea">
            <?php
            // Logic to determine whether or not to display the data to a table or
            // display an error message to the user. If no results are returned
            // This will display an error message and provide the user to go back
            // and edit their form submission.
            if ($numResults < 1) {
                echo "There was no results found ";
                echo "<br>";
                echo "<a href='javascript:history.back()'>Back</a>";
                echo "</div></body></html>";
                die;
                // If there are results display the data
            } else {
                ?>
                <table id="empTable">
                    <tr>
                        <th class="empName">Name</th>
                        <th>Employee Number</th>
                        <th>Hire Date</th>
                        <th>Birth Date</th>
                        <th>Gender</th>
                    </tr>
                    <?php
                    // Loop which will display each result returned from the query in a table
                        while($row = $fNameResult->fetch_assoc()) {
                            ?>
                            <tr>
                                <td class="empName"><?php echo $row['first_name'] . " " . $row['last_name'] ?></td>
                                <td><?php echo "<a href='employeeinfo.php?id=".$row['emp_no']."'>".$row['emp_no']."</a>"; ?></td>
                                <td><?php echo $row['hire_date'] ?></td>
                                <td><?php echo $row['birth_date'] ?></td>
                                <td><?php echo $row['gender'] ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                </table>
                <a href="employee-search.php">Return to search page</a>
                <?php
            }
                    // Free the result memory
                    $fNameResult -> free();
                    // Close the conection to the database
                    $db -> close();
                ?>
        </div>
    </body>
</html>
